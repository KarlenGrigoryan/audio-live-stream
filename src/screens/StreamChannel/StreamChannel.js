import React, {useEffect, useState, Component} from 'react';
import {Text, View, Button, ScrollView} from 'react-native'
import {Actions} from 'react-native-router-flux'
import RtcEngine, {RtcRemoteView, VideoRenderMode} from "react-native-agora";


export class StreamChannel extends Component{
  constructor(props) {
    super(props);
    this.state = {
      configs: {
        peerIds: [],
        uid: new Date().getTime(),
        appid: "d6ab1273dc484594a5405b3982675402",
        channelName: "",
        vidMute: false,
        audMute: false,
        joinSucceed: false,
      },
      _engine: null
    }
  }

  componentDidMount() {
    Actions.refresh({
      onBack: () => {
        console.log('onBack');
        this.leaveChannel();
      }
    });
    this.setState({configs: {...this.state.configs, channelName: this.props.streamInfo.channelName}}, () => {
      this.initRtcEngine()
    });
  }

  leaveChannel = async () => {
    await this.state._engine.leaveChannel();
    this.state._engine.removeAllListeners();
    Actions.pop();
  };

  initRtcEngine = async () => {
    const engine = await RtcEngine.create("d6ab1273dc484594a5405b3982675402");
    this.setState({_engine: engine});
    // Join Channel
    await engine.joinChannel(null, this.state.configs.channelName, `${this.state.configs.uid}`, 0);
    await engine.enableAudio();
    this.addListeners(engine);
  };

  onUserJoin = (uid, data) => {
    const { configs } = this.state;
    const { peerIds } = configs;
    console.log('userjoined: ', uid, configs, 'data:', data);
    console.log('peerIds: ', peerIds);
    //If new user has joined
    if (peerIds.indexOf(Math.abs(uid)) === -1) {
      //add peer ID to state array
      const newConfigs = {
        ...configs,
        peerIds: [ ...peerIds,  Math.abs(uid)]
      };
      console.log('New', newConfigs);
      this.setState({configs: newConfigs});
    }
  };

  addListeners = (engine) => {
    engine.addListener('UserJoined', this.onUserJoin);
    //If user leaves
    engine.addListener('UserOffline', (uid) => {
      const { configs } = this.state;

      console.log('UserOffline', uid);
      this.setState({configs: {...configs, peerIds: configs.peerIds.filter(uid => uid !== uid)}})
    });

    //If Local user joins RTC channel
    engine.addListener('JoinChannelSuccess', (channelName, uid) => {
      const { configs } = this.state;
      console.log('JoinChannelSuccess', uid, 'peerIds:', configs.peerIds);

      //If new user has joined
      if (configs.peerIds.indexOf(uid) === -1) {
        console.log('Add peer Id')
        //add peer ID to state array
        const newConfigs = {
          ...configs,
          peerIds: [...configs.peerIds, uid]
        }
        console.log('join channel newConfigs: ', newConfigs);
        this.setState({configs: newConfigs});
      }
    });

    engine.addListener('LeaveChannel', (data, uuid) => {
      console.log('LeaveChannel', data);
      console.log('uuid: ', uuid);
    })
  };

  mute = async () => {
    const {audMute} = this.state.configs;

    this.setState({configs: {...this.state.configs, audMute: !audMute}});
    console.log('configs.audMute: ', this.state.configs.audMute);
    if (this.state.configs.audMute) {
      // await _engine.disableAudio()
    } else {
      // await _engine.enableAudio()
    }
  };

  render() {
    const {configs} = this.state;
    console.log('Render', configs.peerIds, configs);
    return(
      <View>
        <Text>Joined users count: {configs.peerIds.length}</Text>
        <Button title={configs.audMute ? "mute" : "unmute"} onPress={() => {this.mute()}} />
        <Button title="Leave" onPress={() => {this,leaveChannel()}} />
        <ScrollView>
          {configs.peerIds.map((value, index, array) => {
            return (
              <RtcRemoteView.SurfaceView
                key={index}
                uid={value}
                channelId={configs.channelName}
                renderMode={VideoRenderMode.Hidden}
                zOrderMediaOverlay={true}/>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

import {StyleSheet} from "react-native";

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  form: {
    width: '70%',
    alignItems: 'center',
  },
  input: {
    borderColor: '#ccc',
    borderWidth: 1,
    width: '100%',
    paddingHorizontal: 5
  },
  errorTxt: {
    color: 'red'
  },
  joinBtn: {
    backgroundColor: 'blue',
    marginTop: 20,
    padding: 10,
    borderRadius: 10,
    alignItems: 'center',
    width: '50%'
  },
  joinBtnTxt: {
    color: 'white'
  }
});

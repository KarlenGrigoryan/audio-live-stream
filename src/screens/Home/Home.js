import React, {useState, useEffect} from 'react';
import {View, Text, TextInput, Platform, Button, TouchableOpacity} from 'react-native';
import RtcEngine from "react-native-agora";
import { useForm, Controller } from 'react-hook-form';
import {Actions} from 'react-native-router-flux'

import requestCameraAndAudioPermission from "../../permission";
import styles from './Home.style';

export function HomeScreen() {
  const [configs, setConfigs] = useState({
    peerIds: [],
    uid: `${Math.floor(Math.random() * 100)}`,
    appid: "d6ab1273dc484594a5405b3982675402",
    channelName: "test",
    vidMute: false,
    audMute: false,
    joinSucceed: false,
  });

  const { control, handleSubmit, errors } = useForm();

  const onSubmit = ({channelName})=> {
    Actions.streamChannel({
      streamInfo: {
        channelName
      },
      title: `You join to ${channelName} channel`
    })
  };
  console.log('errors: ', errors);
  return(
    <View style={styles.container}>
      <View style={styles.form}>
        <Controller
          name="channelName"
          defaultValue=""
          control={control}
          rules={{
            required: "This field is required",
            pattern: {
              value: /^[a-zA-Z]{3,}$/,
              message: "This field must only contain characters and the minimum length is 3"
            }
          }}
          render={(props) => (
            <TextInput
              {...props}
              placeholder={"Channel Name"}
              style={styles.input}
              onChangeText={(value) => {
                props.onChange(value)
              }}
            />
          )}
        />
        {
          errors.channelName ? <Text style={styles.errorTxt}>{errors.channelName.message}</Text> : null
        }
        <TouchableOpacity
          style={styles.joinBtn}
          onPress={handleSubmit(onSubmit)}>
          <Text style={styles.joinBtnTxt}>Join</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

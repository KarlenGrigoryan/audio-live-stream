import React, {useState, useEffect} from 'react';
import { Router, Scene, ActionConst } from 'react-native-router-flux';

import { StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, Keyboard, NativeModules, KeyboardAvoidingView, TouchableWithoutFeedback, Dimensions, ScrollView } from 'react-native';
import RtcEngine, {RtcLocalView, RtcRemoteView, VideoRenderMode} from 'react-native-agora';

import requestCameraAndAudioPermission from "./src/permission";
import {HomeScreen} from "./src/screens/Home/Home";
import {StreamChannel} from "./src/screens/StreamChannel/StreamChannel";
// const { Agora } = NativeModules;
// const {
//   AudioProfileDefault,
//   AudioScenarioDefault,
//   Adaptative,
// } = Agora;

export default function App() {

  useEffect(() => {
    //Request required permissions from Android
    if (Platform.OS === 'android') {
      requestCameraAndAudioPermission().then(_ => {
        console.log('requested!');
      });
    }
  });

  return (
    <Router>
      <Scene>
        <Scene key="home" component={HomeScreen} initial title="Audio live streaming" type={ActionConst.PUSH} />
        <Scene key="streamChannel" component={StreamChannel} type={ActionConst.PUSH} back={true} />
      </Scene>
    </Router>

    // <KeyboardAvoidingView style={styles.container} behavior={Platform.OS == "ios" ? "padding" : "height"}>
    //   <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    //     <View>
    //       <TextInput
    //         style={styles.formInput}
    //         value={configs.channelName}
    //         onChangeText={(channelName) => setConfigs({...configs, channelName})}
    //       />
    //       <TouchableOpacity
    //         style={styles.submitButton}
    //         title="Start Call!"
    //         onPress={handleSubmit}
    //       >
    //         <Text style={{ color: '#ffffff' }}> Start Audio </Text>
    //       </TouchableOpacity>
    //       <View style={styles.fullView}>
    //         <ScrollView style={styles.remoteContainer}>
    //           {configs.peerIds.map((value, index, array) => {
    //             return (
    //               <RtcRemoteView.SurfaceView
    //                 key={index}
    //                 style={styles.remote}
    //                 uid={value}
    //                 channelId={configs.channelName}
    //                 renderMode={VideoRenderMode.Hidden}
    //                 zOrderMediaOverlay={true}/>
    //             )
    //           })}
    //         </ScrollView>
    //       </View>
    //     </View>
    //   </TouchableWithoutFeedback>
    //
    // </KeyboardAvoidingView>

  );
}

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formInput: {
    height: 40,
    backgroundColor: '#f5f5f5',
    color: '#0093E9',
    borderRadius: 4,
    paddingLeft: 20,
  },
  submitButton: {
    paddingHorizontal: 60,
    paddingVertical: 10,
    backgroundColor: '#0093E9',
    borderRadius: 25,
  },
  max: {
    flex: 1,
  },
  buttonHolder: {
    height: 100,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: '#0093E9',
    borderRadius: 25,
  },
  buttonText: {
    color: '#fff',
  },
  fullView: {
    width: dimensions.width,
    height: dimensions.height - 100,
  },
  remoteContainer: {
    width: '100%',
    height: 150,
    position: 'absolute',
    top: 5
  },
  remote: {
    width: 150,
    height: 150,
    marginHorizontal: 2.5
  },
  noUserText: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: '#0093E9',
  },
});
